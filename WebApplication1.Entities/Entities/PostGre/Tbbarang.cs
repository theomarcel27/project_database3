﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Entities.Entities.PostGre
{
    [Table("tbbarang")]
    public partial class Tbbarang
    {
        [Key]
        [Column("kodebarang")]
        [StringLength(3)]
        public string Kodebarang { get; set; }
        [Column("judulbarang")]
        [StringLength(20)]
        public string Judulbarang { get; set; }
        [Column("namabenda")]
        [StringLength(20)]
        public string Namabenda { get; set; }
    }
}
